# Clean Architecture

Some guidelines from the book "Clean Architecture" from Robert C. Martin.

<center><b>Take them with a grain of salt</b></center>

--- 

## PART I Introduction


### Chapter 1 - What Is Design and Architecture?

#### **The Goal?**

> The goal of software architecture is to minimize the human resources required to build and maintain the required system.

### Chapter 2 - A Tale of Two Values

> Every software structure provides two different values to the stakeholders: behavior and structure.

#### **Behavior**

> Programmers help the stakeholders to develop a functional specification, or requirements document. Then, we write the code that causes the stakeholder's machines to satisfy those requirements.

#### **Architecture**

> The second value of software has to do with the word "software" - a compound word composed of "soft" and "ware". The word "ware" means "product"; the word "soft" (...) If we wanted the behavior of machines to be hard to change, we would have called it "hardware".

#### **The Greater Value**

Architecture is the greater value.

**Proof**
* "_If you give me a program that works perfectly but is impossible to change, then it won't work when the requirements change, and I won't be able to make it work. Therefore the program will become useless._"
* "_If you give me a program that does not work but is easy to change, then I can make it work, and keep it working as requirements change. Therefore the program  will remain continually useful._"

#### **Eisenhower’s Matrix**

* "_I have two kinds of problems, the urgent and the important. The urgent are not important, and the important are never urgent._"

|                         |                             |
| :---------------------: | :-------------------------: |
| Important && Urgent     | Important && NOT Urgent     |
| NOT Important && Urgent | NOT Important && NOT Urgent |

* The first value of software - behavior - is urgent but not _always_ particularly important.
* The second value of software - architecture - is important but never particularly urgent.
* Somethings are urgent _and_ important. Other things are _neither_ important nor urgent.

Priority
1. Urgent and important
2. Not urgent and important
3. Urgent and not important
4. Not urgent and not important

> Don't elevate items in (3) to priority (1)

#### **Fight for the Architecture**

> Remember, as a software developer, _you are a stakeholder_.

---
 
## PART II Starting with the Bricks: Programming Paradigms


### Chapter 3 - Paradigm Overview

> There are three main Programming Paradigm since Alan Turing laid the foundations of what was to become computer programming. (...) These three paradigms align with the three big concerns of architecture: function, separation of components, and data management.

---

#### **Structured Programming**

> Structured programming imposes discipline on direct transfer of control.

* Invented by Edsger Wybe Dijkstra. To prove the technique, he wrote simple algorithms and realized that **some** `goto` statements prevent modules from being decomposed recursively into smaller and smaller units, thereby preventing use of the divide-and-conquer approach necessary for reasonable proofs.
* Other `goto` statements corresponded to simple selection and iteration control structures such as `if/then/else` and `do/while/until`.
* Together with sequential execution, these concepts became the pillars of structured programming: **sequence, selection, and iteration**.

---

#### **Object-Oriented Programming**

> Object-oriented programming imposes discipline on indirect transfer of control.

* OOP is **not** "The combination of data and function."
* OOP is **not** "A way to model the real world".
* OOP is **not** _encapsulation_, _inheritance_, and _polymorphism_.

##### **Encapsulation**

> The C language had **perfect encapsulation**.

```c
// point.h
struct Point;
struct Point* makePoint(double x, double y);
double distance (struct Point* p1, struct Point* p2);
```

```c
// point.c
#include "point.h"
#include <stdlib.h>
#include <math.h>

struct Point {
    double x, y;
};

struct Point* makePoint(double x, double y) {
    struct Point* p = malloc(sizeof(struct Point));
    p->x = x;
    p->y = y;
    return p;
}

double distance(struct Point* p1, struct Point* p2) {
    double dx = p1->x - p2->x;
    double dy = p1->y - p2->y;
    return sqrt(dx * dx + dy * dy);
}
```

* **The users of `point.h` have no access whatsoever to the members of `struct Point`.**

> But then came OO in the form of C++ &mdash; and the perfect encapsulation of C was broken.

```cpp
// point.hpp
class Point {
    public:
        Point(double x, double y);
        double distance(const Point& p) const;

    private:
        double x;
        double y;
};
```

```cpp
// point.cpp
#include "point.hpp"
#include <math.h>

Point::Point(double x, double y) ; x(x), y(y) {}

double Point::distance(const Point& p) {
    double dx = x - p->x;
    double dy = y - p->y;
    return sqrt(dx * dx + dy * dy);
}
```
Java and C# have even worse encapsulation since there is no separation between header and source file.

##### **Inheritance**

> Inheritance is simply the redeclaration of a group of variables and functions within an enclosing scope.

Let's see a C trick. Consider the following addition to the previous C program.

```c
// namedPoint.h
struct NamedPoint;
struct NamedPoint* makeNamedPoint(double x, double y, char* name);
void setName(struct NamedPoint* np, char* name);
char* getName(struct NamedPoint* np);
```

```c
// namedPoint.c
#include "namedPoint.h"
#include <stdlib.h>

struct NamedPoint {
    double x, y;
    char* name;
};

struct NamedPoint* makeNamedPoint(double x, double y, char* name) {
    struct NamedPoint* p = malloc(sizeof(struct NamedPoint));
    p->x = x;
    p->y = y;
    p->name = name;
    return p;
}
```


```c
// main.c
#include "point.h"
#include "namedPoint.h"
#include <stdio.h>

int main(int ac, char** av) {
    struct NamedPoint* origin = makeNamedPoint(0.0, 0.0, "origin");
    struct NamedPoint* upperRight =
        makeNamedPoint(1.0, 1.0, "upperRight");
    
    printf("distance = %f\n",
            distance(
                (struct Point*) origin,
                (struct Point*) upperRight
            )
    );
}
```

##### **Polymorphism**

Consider this simple C `copy` program.

```c
// copy.c
#include <stdio.h>
void copy() {
    int c;
    while ((c = getchar()) != EOF)
        putchar(c);
}
```
> The UNIX operating system requires that every IO device driver provide five standard functions: `open`, `close`, `read`, `write`, and `seek`. The signatures must be identical for every IO driver.
The `FILE` data structure contains five pointers to functions. In our example, it might look like this:

```c
struct FILE {
    void (*open)(char* name, int mode);
    void (*close)();
    int (*read)();
    void (*write)(char);
    void (*seek)(long index, int mode);
}
```

> Now if `STDIN` is defined as a `FILE*`, and if it points to the console data structure, then `getchar()` might be implemented this way:

```c
extern struct FILE* STDIN;
int getchar() {
    return STDIN->read();
}
```

> This is how OO works in C++; with every virtual function within a class has a pointer in a table called `vtable` (...). OO languages may not have given us polymorphism, but they have made it much safer and much more convenient.

* Polymorphism gave us **Dependency Inversion**, which gives as _separation of concerns_, which allows us to structure the system into _components_ that can be **deployed independently**, which results in **independent developability**.
* "_OO is the ability, through the use of polymorphism, to gain aboslute control ovedr every source code dependency in the system_."

---

#### **Functional Programming**

> Functional programming imposes discipline upon assignment.

* Values or symbols do not change.

```java
public class Squint {
    public static void main(String args[]) {
        for (int i = 0; i < 25; i++) {
            System.out.println(i * i);
        }
    }
}
```

* The Java program uses a _mutable variable_.

```lisp
(println (take 25 (map (fn [x] (* x x)) (range))))
```

* The `range` function returns a never-ending list of integers starting with 0.
* This list is passed into the `map` functino, which calls the anonymous squaring function on each element, producing a new never-ending list of all the squares.
* The list of squares is passed into the `take` function, which returns a new list with only the 25 elements.
* The `println` function prints its input, which is a list of the first 25 squares of integers.

> Why is this important for Architecture?

* **Immutability and Architecture**
* > All race conditions, deadlock conditions, and concurrent update problmes are due to mutable variables. You cannot have a race condition or a concurrent update problem if no variable is ever updated. You cannot have deadlocks without mutable locks.

* **Segregation of Mutability**
* > Well-structured applications will be segregated into those components that do not mutate variables and those that do. This kind of segregation is supported by the use of appropriate disciplines to protect those mutated variables.

* **Event Sourcing**
* > Event sourcing is a strategy wherein we store the transactions, but not the state. When state is required, we simply apply all the transactions from the beginning of time.
* * Examples: git, blockchain

---

## PART III Design Principles

The history of the SOLID principles is long. It was assembled in the late 1980s by Robert C. Martin, while debating software design principles with others on USENET.

* **SRP**: The Single Responsibility Principle
* **OCP**: The Open-Closed Principle
* **LSP**: The Liskov Substitution Principle
* **ISP**: The Interface Segregation Principle
* **DIP**: THe Dependency Inversion Principle

### The Single Responsibility Principle

> _A module should be responsible to one, and only one, actor._

### The Open-Closed Principle

> _A software artifact should be open for extension but closed for modification._

### The Liskov Substitution Principle

> _If for each object o1 of type S there is an object o2 of type T such that for all programs P defined in terms of T, the behavior of P is unchanged when o1 is substituted for o2, then S is a subtype of T._

```c++
// ISP Violation:
Rectangle r = ...
r.setW(5);
r.setH(2);
assert(r.area() == 10);
```
> If the ... code produced a `Square`, then the assertion would fail.

### The Interface Segregation Principle

The ISP can be seen as something like: "_Don't pay for what you don't use._"

The following example, the `Users` depend on all the operations provided by `OPS`.

|     |     |     |
|:---:|:---:|:---:|
|User1|User2|User3|
|| OPS ||
| op1 | op2 | op3 |

In this next example, each `User` depends on the operation they need.
`U1Ops`, `U2Ops`, and `U3Ops` are `Interfaces` implemented by `OPS`.

|     |     |     |
|:---:|:---:|:---:|
|User1|User2|User3|
|U1Ops|U2Ops|U3Ops|
|| OPS ||
| op1 | op2 | op3 |

In C++, this dependency comes with `#include`. In Java, `import`, etc... It is these `included` declarations in source code that create the source code dependencies that force recompilation and redeployment.

### The Dependency Inversion Principle

> The Dependency Inversion Principle (DIP) tells us that the most flexible systems are those in which source code dependencies refer only to abstractions, not to concretions.

* All `include` statements should refer only to source modules containing interfaces, abstract classes, or some other kind of abstract declaration.

---

## PART IV Component Principles

> If the SOLID principles tell us how to arrange the bricks into walls and rooms, then the component principles tell us how to arrange the rooms into buildings.

### Chapter 12 - Components

> Components are the units of deployment. (...) They are the jar files in Java; the gem files in Ruby; the DLLs in .Net; and so on.

#### **Relocatability**

> The idea behind relocatable binaries is simple: the compiler generates binary code that could be relocated in memory by a smart loader. The loader would be told where to load the relocatable code. The relocatable code was instrumented with flags that told the loader which parts of the loaded data had to be altered to be loaded at the selected address.

> The compiler was also changed to emit the names of the functions as metadata in the relocatable binary. If a program called a library function, the compiler would emit that name as an _external reference_. If a program defined a library function, the compiler would emit that name as an _external definition_. Then the loader could _link_ the external references to the external definitions once it had determined where it had loaded those definitions.

> And the linking loader was born.

#### **Linkers**

> The linking loader allowed programmers to divide their programs up onto seperately compilable and loadable segments. (...) This became slow as the programs became larger and larger. Eventually, the loading and the linking were seprated into two phases. Programmers took the slow  part - the part that did that linking - and put it into a separate application called the _linker_. The output of the linker was a linked relocatable that a relocating loader could load very quickly.

> And so the component plugin architecture was born.

### Chapter 13 - Component Cohesion

> Which classes belong in which components? This is an important decision, and requires guidance from good software engineering principles:
> * **REP**: The Reuse/Release Equivalence Principle
> * **CCP**: The Common Closure Principle
> * **CRP**: The Common Reuse Principle

#### **The Reuse/Release Equivalence Principle**

> _The granule of reuse is the granule of release._

The Reuse/Release Equivalent Principle (REP) is a principle that states that reusable software components should be tracked through a release process and given release numbers, (...) to ensure that all the reused components are compatible with each other.

#### **The Common Closure Principle**

> _Gather into components those classes that change for the same reasons and at the same times. Separate into different components those classes that change at different times and for different reasons_. (...) This is the Single Responsibility Principle restated for components.

> This principle is closely associated with the Open Closed Principle (OCP). (...) Because 100% closure is not attainable, closure must be strategic. We design our classes such that they are closed to the most common kinds of changes that we expect or have experienced. The CCP amplifies this lesson by gathering together into the same component those classes that are closed to the same types of changes.

#### **The Common Reuse Principle**

> _Don't force users of a component to depend on things they don't need._

> The Common Reuse Principle (CRP) is yet another principle that helps us to decide which classes and modules should be placed into a component. It states that classes and modules that tend to be reused together belong in the same component. (...) A simple example might be a container class and its associated iterators.

> The CRP says that classes that are not tightly bound to each other should not be in the same component. 

### Chapter 14 - Component Coupling


> The forces that impinge upon the architecture of a component structure are technical, political, and volatile.

#### **The Acyclic Dependencies Principle**

> _Allow no cycles in the component dependency graph_

#### **Top-Down Design**

> The component structure cannot be designed from the top down. It is not one of the first things about the system that is designed, but rather evolves as the system grows and changes. (...) the component dependency graph is created and molded by architects to protect stable high-value components from volatile components.

> As the application continues to grow, (...), the CRP begins to influence the composition of the components. Finally, as cycles appear, the ADP is applied and the component dependency graph jitters and grows.

> Thus the component dependency structure grows and evolves with the logical design of the system.

#### **The Stable Dependencies Principle**

> _Depend in the direction of stability._

> By conforming to the Stable Dependencies Principle (SDP), we ensure that modules that are intended to be easy to change are not  depended on by mmodules that are harder to change.

Stability can be measured by counting the number of dependencies that enter and leave a component.

* **Fan-in**: Incoming dependencies. This metric identifies the number of classes outside this component that depend on classes within the component.

* **Fan-out**: Outgoing dependencies. This metric identifies the number of classes inside this component that depend on classes outside the component.

* **I**: Instability: `I = Fan-out / (Fan-In + Fan-out)`. This metric has the range [0, 1]. I = 0 indicates a maximally stable component. I = 1 indicates a maximally unstable component.

> The SDP says that the _I_ metric of a component should be larger than the _I_ metric of the components that it depends on. That is, _I_ metrics should _decrease_ in the direction of dependency-

> In C++, these dependencies are typically represented by `#include` statements.

#### **The Stable Abstractions Principle**

> _A component should be as abstract as it is stable._

> The Stable Abstractions Principle (SAP) sets up a relationship between stability and abstractness. On the one hand, it says that a stable component should also be abstract so that its stability does not prevent it from being extended. On the other hand, it says that an unstable component should be concrete since its instability allows the concrete code within it to be easily changed.

> The SAP and the SDP combined amount to the DIP for components. This is true because the SDP says that dependencies should run in the direction of stability, and the SAP says that stability implies abstraction. Thus, _dependencies run in the direction of abstraction._

---

## PART V Architecture

### Chapter 15 - What Is Architecture?

> The architecture of a software system is the shape given to that system by those who build it. The form of that shape is in the division of that system into components, the arrangement of those components, and the ways in which those components communicate with each other.

> The purpose of that shape is to facilitate the **development**, **deployment**, **operation**, and **maintenance** of the software system contained within it.

> _The strategy behind that facilitation is to leave as many options open as possible, for as long as possible._

> The primary purpose of architecture is to support the life cycle of the system. Good architecture makes the system easy to understand, easy to develop, easy to maintain, and easy to deploy. The ultimate goal is to minimize the lifetime cost of the system and to maximize programmer productivity.

#### **Keeping Options Open**

> All software systems can be decomposed into two major elements: policy and details. The policy element embodies all the business rules and procedures. The policy is where the true value of the system lives. The details are thoes things that are necessary to enable humans, other systems, and programmers to communicate with the policy, but that do not impact the behavior of the policy at all.

> The goal of the architect is to create a shape for the system that recognizes policy as the most essential element of the system while making the details _irrelevant_ to that policy.

> * The database is a detail
> * The web is a detail
> * The api is a detail
> * The framework is a detail

> _A good architect maximizes the number of decisions not made._

### Chapter 16 - Independence

#### **Use Cases**

...

#### **Operation**

...

#### **Development**

...

#### **Deployment**

...

#### **Leaving Options Open**

...

#### **Decoupling Layers**

...

#### **Decoupling Use Cases**

...

#### **Decoupling Mode**

...

#### **Independent Develop-ability**

...

#### **Independent Deployability**

...

#### **Duplication**

...

#### **Decoupling Modes (Again)**

...

#### **Conclusion**

...


### Chapter 17 - Boundaries: Drawing Lines

#### **A Couple of Sad Stories**

...

#### **FitNesse**

...

#### **Which Lines Do You Draw, and When Do You Draw Them?**

...

#### **What About Input and Output?**

...

#### **Plugin Architecture**

...

#### **The Plugin Argument**

...

#### **Conclusion**

...


### Chapter 18 - Boundary Anatomy

#### **Boundary Crossing**

...

#### **The Dreaded Monolith**

...

#### **Deployment Components**

...

#### **Threads**

...

#### **Local Processes**

...

#### **Services**

...

#### **Conclusion**

...


### Chapter 19 - Policy and Level

#### **Level**

...

#### **Conclusion**

...


### Chapter 20 - Business Rules

#### **Entities**

...

#### **Use Cases**

...

#### **Request and Response Models**

...

#### **Conclusion**

...


### Chapter 21 - Screaming Architecture

#### **The Theme of an Architecture**

...

#### **The Purpose of an Architecture**

...

#### **But What About the Web?**

...

#### **Frameworks Are Tools, Not Ways of Life**

...

#### **Testable Architectures**

...

#### **Conclusion**

...


### Chapter 22 - The Clean Architecture

#### **The Dependency Rule**

...

#### **A Typical Scenario**

...

#### **Conclusion**

...


### Chapter 23 - Presenters and Humble Objects

#### **The Humble Object Pattern**

...

#### **Presenters and Views**

...

#### **Testing and Architecture**

...

#### **Database Gateways**

...

#### **Data Mappers**

...

#### **Service Listeners**

...

#### **Conclusion**

...


### Chapter 24 - Partial Boundaries

#### **Skip the Last Step**

...

#### **One-Dimensional Boundaries**

...

#### **Facades**

...

#### **Conclusion**

...


### Chapter 25 - Layers and Boundaries

#### **Hunt the Wumpus**

...

#### **Clean Architecture?**

...

#### **Crossing the Streams**

...

#### **Splitting the Streams**

...

#### **Conclusion**

...


### Chapter 26 - The Main Component

#### **The Ultimate Detail**

...

#### **Conclusion**

...


### Chapter 27 - Services: Great and Small

#### **Service Architecture?**

...

#### **Service Benefits?**

...

#### **The Kitty Problem**

...

#### **Objects to the Rescue**

...

#### **Component-Based Services**

...

#### **Cross-Cutting Concerns**

...

#### **Conclusion**

...


### Chapter 28 - The Test Boundary

#### **Tests as System Components**

...

#### **Design for Testability**

...

#### **The Testing API**

...

#### **Conclusion**

...


### Chapter 29 - Clean Embedded Architecture

#### **App-titude Test**

...

#### **The Target-Hardware Bottleneck**

...

#### **Conclusion**

--- 

## PART VI Details

### Chapter 30 - The Database Is a Detail

#### **Relational Databases**

...

#### **Why Are Database Systems So Prevalent?**

...

#### **What If There Were No Disk?**

...

#### **Details**

...

#### **But What about Performance?**

...

#### **Anecdote**

...

#### **Conclusion**

...


### Chapter 31 - The Web Is a Detail

#### **The Endless Pendulum**

...

#### **The Upshot**

...

#### **Conclusion**

...


### Chapter 32 - Frameworks Are Details

#### **Framework Authors**

...

#### **Asymmetric Marriage**

...

#### **The Risks**

...

#### **The Solution**

...

#### **I Now Pronounce You …**

...

#### **Conclusion**

...


### Chapter 33 - Case Study: Video Sales

#### **The Product**

...

#### **Use Case Analysis**

...

#### **Component Architecture**

...

#### **Dependency Management**

...

#### **Conclusion**

...


### Chapter 34 - The Missing Chapter

#### **Package by Layer**

...

#### **Package by Feature**

...

#### **Ports and Adapters**

...

#### **Package by Component**

...

#### **The Devil Is in the Implementation Details**

...

#### **Organization versus Encapsulation**

...

#### **Other Decoupling Modes**

...

#### **Conclusion: The Missing Advice**

...

#### **Afterword**


--- 

## PART VII Appendix

#### **Appendix A Architecture Archaeology**

...

