# Clean Code

### Chapter 1: Clean Code

#### There Will Be Code

...

#### Bad Code

...

#### The Total Cost of Owning a Mess

...

#### Schools of Thought

...

#### We Are Authors

...

#### The Boy Scout Rule

...

#### Prequel and Principles

...

#### Conclusion

...

#### Bibliography

...

### Chapter 2: Meaningful Names

#### Introduction

...

#### Use Intention-Revealing Names

...

#### Avoid Disinformation

...

#### Make Meaningful Distinctions

...

#### Use Pronounceable Names

...

#### Use Searchable Names

...

#### Avoid Encodings

...

#### Avoid Mental Mapping

...

#### Class Names

...

#### Method Names

...

#### Don’t Be Cute

...

#### Pick One Word per Concept

...

#### Don’t Pun

...

#### Use Solution Domain Names

...

#### Use Problem Domain Names

...

#### Add Meaningful Context

...

#### Don’t Add Gratuitous Context

...

#### Final Words

...

### Chapter 3: Functions

#### Small!

...

#### Do One Thing

...

#### One Level of Abstraction per Function

...

#### Switch Statements

...

#### Use Descriptive Names

...

#### Function Arguments

...

#### Have No Side Effects

...

#### Command Query Separation

...

#### Prefer Exceptions to Returning Error Codes

...

#### Don’t Repeat Yourself

...

#### Structured Programming

...

#### How Do You Write Functions Like This?

...

#### Conclusion

...

#### SetupTeardownIncluder

...

#### Bibliography

...

### Chapter 4: Comments

#### Comments Do Not Make Up for Bad Code

...

#### Explain Yourself in Code

...

#### Good Comments

...

#### Bad Comments

...

#### Bibliography

...

### Chapter 5: Formatting

#### The Purpose of Formatting

...


#### Vertical Formatting

...


#### Horizontal Formatting

...


#### Team Rules

...


#### Uncle Bob’s Formatting Rules

...

### Chapter 6: Objects and Data Structures

#### Data Abstraction

...

#### Data/Object Anti-Symmetry

...

#### The Law of Demeter

...

#### Data Transfer Objects

...

#### Conclusion

...

#### Bibliography

...

### Chapter 7: Error Handling

#### Use Exceptions Rather Than Return Codes

...

#### Write Your Try-Catch-Finally Statement First

...

#### Use Unchecked Exceptions

...

#### Provide Context with Exceptions

...

#### Define Exception Classes in Terms of a Caller’s Needs

...

#### Define the Normal Flow

...

#### Don’t Return Null

...

#### Don’t Pass Null

...

#### Conclusion

...

#### Bibliography

...

### Chapter 8: Boundaries

#### Using Third-Party Code

...

#### Exploring and Learning Boundaries

...

#### Learning log4j

...

#### Learning Tests Are Better Than Free

...

#### Using Code That Does Not Yet Exist

...

#### Clean Boundaries

...

#### Bibliography

...

### Chapter 9: Unit Tests

#### The Three Laws of TDD

...

#### Keeping Tests Clean

...

#### Clean Tests

...

#### One Assert per Test

...

#### F.I.R.S.T.

...

#### Conclusion

...

#### Bibliography

...

### Chapter 10: Classes

#### Class Organization

...

#### Classes Should Be Small!

...

#### Organizing for Change

...

#### Bibliography

...

### Chapter 11: Systems

#### How Would You Build a City?

...

#### Separate Constructing a System from Using It

...

#### Scaling Up

...

#### Java Proxies

...

#### Pure Java AOP Frameworks

...

#### AspectJ Aspects

...

#### Test Drive the System Architecture

...

#### Optimize Decision Making

...

#### Use Standards Wisely, When They Add Demonstrable Value

...

#### Systems Need Domain-Specific Languages

...

#### Conclusion

...

#### Bibliography

...

### Chapter 12: Emergence

#### Getting Clean via Emergent Design

...

#### Simple Design Rule 1: Runs All the Tests

...

#### Simple Design Rules 2–4: Refactoring

...

#### No Duplication

...

#### Expressive

...

#### Minimal Classes and Methods

...

#### Conclusion

...

#### Bibliography

...

### Chapter 13: Concurrency

#### Why Concurrency?

...

#### Challenges

...

#### Concurrency Defense Principles

...

#### Know Your Library

...

#### Know Your Execution Models

...

#### Beware Dependencies Between Synchronized Methods

...

#### Keep Synchronized Sections Small

...

#### Writing Correct Shut-Down Code Is Hard

...

#### Testing Threaded Code

...

#### Conclusion

...

#### Bibliography

...

### Chapter 14: Successive Refinement

#### Args Implementation

...

#### Args: The Rough Draft

...

#### String Arguments

...

#### Conclusion

...

### Chapter 15: JUnit Internals

#### The JUnit Framework

...

#### Conclusion

...

### Chapter 16: Refactoring SerialDate

#### First, Make It Work

...

#### Then Make It Right

...

#### Conclusion

...

#### Bibliography

...

### Chapter 17: Smells and Heuristics

#### Comments

...

#### Environment

...

#### Functions

...

#### General

...

#### Java

...

#### Names

...

#### Tests

...

#### Conclusion

...

#### Bibliography

...

### Appendix A: Concurrency II

#### Client/Server Example

...

#### Possible Paths of Execution

...

#### Knowing Your Library

...

#### Dependencies Between Methods Can Break Concurrent Code

...

#### Increasing Throughput

...

#### Deadlock

...

#### Testing Multithreaded Code

...

#### Tool Support for Testing Thread-Based Code

...

#### Conclusion

...

#### Tutorial: Full Code Examples

...

### Appendix B: org.jfree.date.SerialDate

### Appendix C: Cross References of Heuristics
