#!/usr/bin/env bash

# =========================================================================== #
# Logger Setup
# =========================================================================== #
# Include Logger
DIRNAME=$(dirname "$0")
# shellcheck disable=SC1090
. "${DIRNAME}"/Logger.sh

SCRIPT_NAME=$(awk -F/ '{printf $NF}' <<<"$0")
CMAKE_CURRENT_LOG_LEVEL=${CMAKE_LOG_LEVEL_INFO}

function _LogDebug() {
    if [ "${CMAKE_CURRENT_LOG_LEVEL}" -le "${CMAKE_LOG_LEVEL_DEBUG}" ]; then
        LogDebug "${SCRIPT_NAME}" "$1"
    fi
}
function _LogInfo() {
    if [ "${CMAKE_CURRENT_LOG_LEVEL}" -le "${CMAKE_LOG_LEVEL_INFO}" ]; then
        LogInfo "${SCRIPT_NAME}" "$1"
    fi
}
function _LogWarn() {
    if [ "${CMAKE_CURRENT_LOG_LEVEL}" -le "${CMAKE_LOG_LEVEL_WARNING}" ]; then
        LogWarn "${SCRIPT_NAME}" "$1"
    fi
}
function _LogError() {
    if [ "${CMAKE_CURRENT_LOG_LEVEL}" -le "${CMAKE_LOG_LEVEL_ERROR}" ]; then
        LogError "${SCRIPT_NAME}" "$1"
    fi
}

# =========================================================================== #
# Initial Setup
# =========================================================================== #

# if any command inside script returns error, exit and return that error
set -e

# Change Directory to project root folder
cd "${0%/*}/../../.."
ROOT_DIRECTORY=$(pwd)

# Tests do not depend on the libraries CMAKE_CONFIGURATION_TYPE (LIB-64, ...),
# therefore, they should be on a separate folder structure
TEST_DIR=build/Linux/Tests-CI
DIST_DIR=dist/tests

CONFIGURATION_TYPE=CI
ARCHITECTURE_SUFFIX=32

CMAKE_BUILD_DIRECTORY=${TEST_DIR}/${CONFIGURATION_TYPE}-${ARCHITECTURE_SUFFIX}
CMAKE_RUNTIME_DIRECTORY=${DIST_DIR}/${CONFIGURATION_TYPE}-${ARCHITECTURE_SUFFIX}/bin

# Check the command line arguments
while test $# -gt 0; do
    case $1 in
    -a | --all)
        shift
        _LogWarn "Cleaning previous builds"
        rm -rf ${TEST_DIR}/CI-32/
        rm -rf ${DIST_DIR}/CI-32/bin
        rm -rf ${TEST_DIR}/CI-64/
        rm -rf ${DIST_DIR}/CI-64/bin
        ;;
    -v | --verbose)
        shift
        CMAKE_CURRENT_LOG_LEVEL=${CMAKE_LOG_LEVEL_DEBUG}
        ;;
    \?)
        shift
        echo "Invalid option: -$OPTARG"
        exit 1
        ;;
    esac
done


_LogInfo "Running run-tests script..."
_LogDebug "$(pwd)"
_LogDebug "TEST_DIR: ${TEST_DIR}"
_LogDebug "CONFIGURATION_TYPE: ${CONFIGURATION_TYPE}"
_LogDebug "CMAKE_BUILD_DIRECTORY: ${CMAKE_BUILD_DIRECTORY}"

# =========================================================================== #
# CMake Setup
# =========================================================================== #
function generate_build_system() {
    _LogInfo "Generating Build System"
    _LogDebug "$(pwd)"

    _LogDebug "Running cmake . -B ${CMAKE_BUILD_DIRECTORY} -DCMAKE_CURRENT_LOG_LEVEL=${CMAKE_CURRENT_LOG_LEVEL}"
    cmake . -B ${CMAKE_BUILD_DIRECTORY} -DCMAKE_CURRENT_LOG_LEVEL=${CMAKE_CURRENT_LOG_LEVEL} -DCMAKE_BUILD_TYPE=${CONFIGURATION_TYPE} -DCMAKE_CXX_COMPILER=/usr/bin/g++-4.8

    _LogInfo "Generating Build System - done"
}

# detect CMake BYPRODUCTS (CMakeFiles/ CMakeCache.txt Makefile)
function detect_cmake_installation() {
    _LogInfo "Detecting CMake Installation"

    # [[ UNLIKELY(Should be created by run_conan_installation) ]]
    if [ ! -d "${CMAKE_BUILD_DIRECTORY}" ]; then
        _LogError "Detecting CMake Installation - NOT FOUND"
        #run_conan_installation
        cd ${ROOT_DIRECTORY}
        generate_build_system
    else
        cd ${CMAKE_BUILD_DIRECTORY}
        _LogInfo "Detecting Makefile"
        # Using Makefile for detection
        if [ ! -f "Makefile" ]; then
            _LogInfo "Detecting Makefile - NOT FOUND"
            cd ${ROOT_DIRECTORY}
            # cmake runs from root folder
            generate_build_system
        else
            _LogInfo "Detecting Makefile - done"
            cd ${ROOT_DIRECTORY}
            _LogInfo "Detecting CMake Installation - done"
        fi
    fi
}

detect_cmake_installation

# =========================================================================== #
# Build Setup
# =========================================================================== #
function run_build_system() {
    _LogInfo "Building Project Tests"
    _LogDebug "$(pwd)"

    if [ "${CMAKE_CURRENT_LOG_LEVEL}" = "${CMAKE_LOG_LEVEL_DEBUG}" ]; then
        _LogDebug "Running cmake --build ${CMAKE_BUILD_DIRECTORY} --target bingomath_core_library_unit_tests -- --no-print-directory VERBOSE=1 -j 4"
        cmake --build ${CMAKE_BUILD_DIRECTORY}/ --target bingomath_core_library_unit_tests -- --no-print-directory VERBOSE=1 -j 4

        _LogDebug "Running cmake --build ${CMAKE_BUILD_DIRECTORY} --target bingomath_core_library_acceptance_tests -- --no-print-directory VERBOSE=1 -j 4"
        cmake --build ${CMAKE_BUILD_DIRECTORY}/ --target bingomath_core_library_acceptance_tests -- --no-print-directory VERBOSE=1 -j 4
    else
        _LogDebug "Running cmake --build ${CMAKE_BUILD_DIRECTORY} --target bingomath_core_library_unit_tests -- --no-print-directory VERBOSE=1 -j 4"
        cmake --build ${CMAKE_BUILD_DIRECTORY}/ --target bingomath_core_library_unit_tests -- --no-print-directory -j 4

        _LogDebug "Running cmake --build ${CMAKE_BUILD_DIRECTORY} --target bingomath_core_library_acceptance_tests -- --no-print-directory VERBOSE=1 -j 4"
        cmake --build ${CMAKE_BUILD_DIRECTORY}/ --target bingomath_core_library_acceptance_tests -- --no-print-directory -j 4
    fi

    _LogInfo "Building Project Tests - done"
}

run_build_system

# =========================================================================== #
# CTest Setup
# =========================================================================== #
function run_ctest() {
    echo ""
    _LogInfo "Running CTest"
    cd ${CMAKE_BUILD_DIRECTORY}
    _LogDebug "$(pwd)"
    if [ "${CMAKE_CURRENT_LOG_LEVEL}" = "${CMAKE_LOG_LEVEL_DEBUG}" ]; then
        ctest --output-on-failure -VV
    else
        ctest --output-on-failure
    fi

    if [ $? != 0 ]; then
        echo ""
        _LogError "---------------------------------------------------------- Tests failed"
        echo ""
        exit 1
    fi
    echo ""
    _LogInfo "------------------------------------------------------- Tests succeeded"
    echo ""

    cd ${ROOT_DIRECTORY}

}

run_ctest

#64 bits

ARCHITECTURE_SUFFIX=64

CMAKE_BUILD_DIRECTORY=${TEST_DIR}/${CONFIGURATION_TYPE}-${ARCHITECTURE_SUFFIX}
CMAKE_RUNTIME_DIRECTORY=${DIST_DIR}/${CONFIGURATION_TYPE}-${ARCHITECTURE_SUFFIX}/bin

detect_cmake_installation
run_build_system
run_ctest


exit
