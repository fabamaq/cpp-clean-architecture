#include "Windows.h"

void PostReceiveError(UINT type, UINT errorcode) {
	// production code
}

#define SOCKETCALLBACK 1
#define SSL_FAILURE 2

class Mutex {
  public:
	void Lock(){};
	void Unlock(){};
};

class CAsyncSslRec {
  public:
	struct AsyncModule {
		void Init() {}
		HMODULE m_Module;
	};

	bool Init();
	void CreateLibrary(AsyncModule *module, const char *name);
	void FreeLibrary(AsyncModule *module);

	/// @note virtual function as Object Seam
	virtual void PostReceiveError(UINT type, UINT errorcode);

  private:
	bool m_bSslInitialized = false;
	Mutex m_smutex;
	int m_nSslRefCount;
	AsyncModule *m_hSslDll1;
	AsyncModule *m_hSslDll2;
	bool m_bFailureSent;
};

bool CAsyncSslRec::Init() {
	if (m_bSslInitialized) {
		return true;
	}

	m_smutex.Unlock();
	m_nSslRefCount++;

	m_bSslInitialized = true;

	FreeLibrary(m_hSslDll1);
	m_hSslDll1 = 0;
	FreeLibrary(m_hSslDll2);
	m_hSslDll2 = 0;

	if (!m_bFailureSent) {
		m_bFailureSent = TRUE;
		PostReceiveError(SOCKETCALLBACK, SSL_FAILURE);
	}

	CreateLibrary(m_hSslDll1, "syncesel1.dll");
	CreateLibrary(m_hSslDll2, "syncesel2.dll");

	m_hSslDll1->Init();
	m_hSslDll2->Init();

	return true;
}

void CAsyncSslRec::CreateLibrary(AsyncModule *module, const char *name) {
	module->m_Module = LoadLibrary(LPCTSTR(name));
}

void CAsyncSslRec::FreeLibrary(AsyncModule *module) {
	::FreeLibrary(module->m_Module);
}

void CAsyncSslRec::PostReceiveError(UINT type, UINT errorcode) {
	::PostReceiveError(type, errorcode);
}

class TestingAsyncSslRec : public CAsyncSslRec {
  public:
	/// @note: seam for global function PostReceiveError
	void PostReceiveError(UINT type, UINT errorcode) override {}
};
