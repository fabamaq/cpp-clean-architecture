#pragma once

constexpr int MAX_ITEMS = 10;
constexpr int MASTER_ACCOUNT = 1;

struct DFHLItem;
struct DHLSRecord {
  bool dateStamped;
  int quantity;
  DFHLItem *item;
  DFHLItem *backup_item;
};