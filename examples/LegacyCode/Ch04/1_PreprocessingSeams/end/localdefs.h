#ifdef TESTING

struct DFHLItem *last_item = NULL;
int last_account_no = -1;

#define db_update(account_no, item)                                            \
  {                                                                            \
    last_item = (item);                                                        \
    last_account_no = (account_no);                                            \
  }
#endif
