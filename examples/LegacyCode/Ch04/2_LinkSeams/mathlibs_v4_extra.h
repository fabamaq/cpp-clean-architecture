/**
 * @file mathlibs_v4_extra.h
 * @brief Extends Game Math API when developing Game Dev logic
 */

#ifndef mathlibs_v4_extra_h
#define mathlibs_v4_extra_h

extern "C" {

void setProtocolVersion(int newVersion);



} // extern "C"

#endif // mathlibs_v4_extra_h