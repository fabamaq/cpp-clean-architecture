set(CMAKE_POSITION_INDEPENDENT_CODE ON)

add_library(Math SHARED)
target_sources(Math PRIVATE mathlibs_v4.cpp)
target_include_directories(Math PRIVATE ${ROOT_DIR}/include ${ROOT_DIR}/lib)
set_target_properties(Math PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${ROOT_DIR}/src)

add_executable(GAME)
target_sources(
  GAME PRIVATE game.cpp $<TARGET_OBJECTS:global_precompiled_headers>
)
target_include_directories(GAME PRIVATE ${ROOT_DIR}/include ${ROOT_DIR}/lib)
target_link_libraries(GAME PRIVATE dl)
set_target_properties(GAME PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${ROOT_DIR}/src)
add_dependencies(GAME Math)
