#include "main.hpp"

#include <catch2/catch.hpp>

class SerialTask {
  public:
    virtual void run();
    // ...
};

class ISchedulingTask {
  public:
    virtual void run() = 0;
};

class SchedulerPane {
  public:
    virtual ~SchedulerPane() = default;
};

class SchedulingTask : public SerialTask, public ISchedulingTask {
  public:
    SchedulingTask(Scheduler scheduler, MeetingResolver resolver) {
        // ...
    }
};

class SchedulingTaskPane : public SchedulerPane {
  public:
    virtual void run() {
        SerialTask::run();
    }
    SchedulingTaskPane(SchedulingTask task) {
        // ...
    }
};

TEST_CASE("The Case of the Onion Parameter", "[begin]") {
    // Nightmare to isntantiate SchedulingTaskPane
}
