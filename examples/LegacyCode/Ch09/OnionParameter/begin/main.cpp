#include "main.hpp"

#include <catch2/catch.hpp>

class SchedulerPane {
  public:
    virtual ~SchedulerPane() = default;
};

class SchedulingTask : public SerialTask {
  public:
    SchedulingTask(Scheduler scheduler, MeetingResolver resolver) {
        // ...
    }
};

class SchedulingTaskPane : public SchedulerPane {
  public:
    SchedulingTaskPane(SchedulingTask task) {
        // ...
    }
};

TEST_CASE("The Case of the Onion Parameter", "[begin]") {
    // Nightmare to isntantiate SchedulingTaskPane
}
