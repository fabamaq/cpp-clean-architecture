#include "main.hpp"

#include <catch2/catch.hpp>

class IndustrialFacility : public Facility {
  public:
    IndustrialFacility(
        int facilityCode, String owner, OriginationPermit permit) /* throws PermitViolation */
        : Facility(facilityCode, owner) {
        Permit associatedPermit =
            PermitRepository::getInstance().findAssociatedFromOrigination(permit);

        if (associatedPermit.isValid() && !permit.isValid()) {
            basePermit = associatedPermit;
        } else if (!permit.isValid()) {
            permit.validate();
            basePermit = permit;
        } else {
            throw new PermitViolation(permit);
        }
    }
};

TEST_CASE("The Case of the Aliased Parameter", "[begin]") {
    // Nightmare to isntantiate SchedulingTaskPane
}
