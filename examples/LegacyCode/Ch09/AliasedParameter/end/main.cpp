#include "main.hpp"

#include <catch2/catch.hpp>

class IndustrialFacility : public Facility {
  public:
    IndustrialFacility(
        int facilityCode, String owner, OriginationPermit& permit) /* throws PermitViolation */
        : Facility(facilityCode, owner, permit) {
        Permit associatedPermit =
            PermitRepository::getInstance().findAssociatedFromOrigination(permit);

        if (associatedPermit.isValid() && !permit.isValid()) {
            basePermit = associatedPermit;
        } else if (!permit.isValid()) {
            permit.validate();
            basePermit = permit;
        } else {
            throw new PermitViolation(permit);
        }
    }
};

class FakeOriginationPermit : public OriginationPermit {
  public:
    virtual void validate() {}
    virtual void becomeValid() {}
};

TEST_CASE("The Case of the Aliased Parameter", "[testHasPermit][end]") {
    class AlwaysValidPermit : public FakeOriginationPermit {
      public:
        void validate() override {
            // set the validation flag
            becomeValid();
        }
    };

    Facility* facility = new IndustrialFacility(Facility::HT_1, "b", *(new AlwaysValidPermit()));

    delete facility;
}
