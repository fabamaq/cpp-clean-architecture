#pragma once

#include <string>
using String = std::string;

class PermitNotice {
  public:
  virtual ~PermitNotice() = default;
    PermitNotice() = default;
    PermitNotice(int newCode, String newName)
        : code(newCode)
        , name(newName) {}
    bool isValid() {
        return true;
    }

    int code;
    String name;
};

class Permit : public PermitNotice {
  public:
    Permit() = default;
    virtual ~Permit() = default;
    Permit(PermitNotice notice)
        : mNotice(notice) {}
    bool isValid() {
        return true;
    }

    virtual void validate() {
        // form connection to database
        // query for validation information
        // set the validation flag
        // close database
    }

    PermitNotice mNotice;
};

class FacilityPermit : public Permit {
  public:
    virtual ~FacilityPermit() = default;
    void validate() override {
        // form connection to database
        // query for validation information
        // set the validation flag
        // close database
    }
};

class OriginationPermit : public FacilityPermit {
  public:
    void validate() override {
        // form connection to database
        // query for validation information
        // set the validation flag
        // close database
    }
};

struct PermitNoticeHash {
    std::size_t operator()(const PermitNotice& k) const {
        return std::hash<int>{}(k.code);
    }
};

struct PermitNoticeKeyEqual {
    bool operator()(const PermitNotice& lhs, const PermitNotice& rhs) const {
        return lhs.code == rhs.code;
    }
};

#include <unordered_map>
//          std::unordered_map<Key,T,Hash,KeyEqual,[Allocator]>::unordered_map
using Map = std::unordered_map<PermitNotice, Permit, PermitNoticeHash, PermitNoticeKeyEqual>;

/**
 * @brief The Irritating Global Dependency
 * @implements the Singleton design pattern
 */
class PermitRepository {
    static PermitRepository* sInstance;

  public:
    static PermitRepository& getInstance() {
        if (sInstance == nullptr) {
            sInstance = new PermitRepository();
        }
        return *sInstance;
    }

    static void setTestingInstance(PermitRepository* newInstance) {
        sInstance = newInstance;
    }

    Permit findAssociatedPermit(PermitNotice notice) {
        // open permit database
        // select using values in notice
        // verify we have only one  matching permit, if not report error
        // return matching permit
        return Permit(notice);
    }

    Permit findAssociatedFromOrigination(OriginationPermit notice) {
        // open permit database
        // select using values in notice
        // verify we have only one  matching permit, if not report error
        // return matching permit
        return Permit(notice);
    }

  protected:
    PermitRepository() = default;
};

inline PermitRepository* PermitRepository::sInstance = nullptr;

class Facility {
  public:
    class PermitViolation : public std::runtime_error {
      public:
        explicit PermitViolation(Permit p)
            : std::runtime_error("PermitViolation") {}
    };

    Facility(int facilityCode, String owner, PermitNotice& notice) /* throws PermitViolation */ {
        Permit associatedPermit = PermitRepository::getInstance().findAssociatedPermit(notice);

        if (associatedPermit.isValid() && !notice.isValid()) {
            basePermit = associatedPermit;
        } else if (!notice.isValid()) {
            Permit permit = Permit(notice);
            permit.validate();
            basePermit = permit;
        } else {
            throw new PermitViolation(associatedPermit /* before::permit */);
        }
    }
    // ...

    constexpr static int RESIDENCE = 1;
    constexpr static int HT_1 = 1;

  protected:
    Permit basePermit;
};
