#pragma once
#include "legacyCodePCH.hpp"

constexpr int MAIL_OKAY = 1;
constexpr int MS_AVAILABLE = 2;
constexpr int MAIL_OFFLINE = -1;

constexpr int ML_NOBOUND = 4;
constexpr int ML_REPEATOFF = 5;

constexpr int MARK_MESSAGES_OFF = 9;

class mailing_list_dispatcher;

class mail_service {

  public:
    void connect() {}
    int get_status() {
        return MS_AVAILABLE;
    }
    void register_(mailing_list_dispatcher* dispatcher, int client_type, int flag) {}
    void set_param(int client_type, int flag) {}
};
struct mail_txm_id {};
struct mail_address {};
