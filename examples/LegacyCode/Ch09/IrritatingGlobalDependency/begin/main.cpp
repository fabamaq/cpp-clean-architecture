#include "main.hpp"

#include <catch2/catch.hpp>
#include <stdexcept>

class Facility {
    Permit basePermit;

  public:
    class PermitViolation : public std::runtime_error {
      public:
        explicit PermitViolation(Permit p) : std::runtime_error("PermitViolation") {}
    };

    Facility(int facilityCode, String owner, PermitNotice notice) /* throws PermitViolation */ {
        Permit associatedPermit = PermitRepository::getInstance().findAssociatedPermit(notice);

        if (associatedPermit.isValid() && !notice.isValid()) {
            basePermit = associatedPermit;
        } else if (!notice.isValid()) {
            Permit permit = Permit(notice);
            permit.validate();
            basePermit = permit;
        } else {
            throw new PermitViolation(associatedPermit /* before::permit */);
        }
    }
    // ...

    constexpr static int RESIDENCE = 1;
};

TEST_CASE("The Case of the Irritating Global Dependency", "[testCreate][begin]") {
    PermitNotice notice = PermitNotice(0, "a");
    Facility facility = Facility(Facility::RESIDENCE, "b", notice);
}
