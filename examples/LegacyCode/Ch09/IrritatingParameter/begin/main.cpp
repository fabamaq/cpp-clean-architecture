#include "main.hpp"

#include <catch2/catch.hpp>

/**
 * @brief
 * - Tell whether customers have valid credit.
 * - ... if true, get a certificate for credit amount
 * - ... it false, throw InvalidCredit
 *
 * TODO: add a new getValidationPercent method
 * - to tell the % of successful validateCustomer calls
 * - ... we've made over the life of the validator
 */
class CreditValidator {
  public:
    CreditValidator(RGHConnection connection, CreditMaster master, String validatorID) {
        // ...
    }

  private:
    Certificate validateCustomer(Customer customer) /* throws InvalidCredit */ {
        // ...
    }
};


TEST_CASE("The Case of the Irritating Parameter", "[testCreate][begin]") {
    // Connects to a server and takes too long // ! The Irritating Parameter
    RGHConnection connection = RGHConnection(DEFAULT_PORT, "admin", "rii8ii9s");
    // Loads data from a file but is quick; and is readonly so, no racing or corruption
    CreditMaster master = CreditMaster("crm2.mas", true);
    CreditValidator validator = CreditValidator(connection, master, "a");
}
