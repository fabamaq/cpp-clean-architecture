#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "DayTime.h"
#include "MailDaemon.h"
// ...
#include "Date.h"
#include "Event.h"
// ...
#include "Meeting.h"
#include "SchedulerDisplay.h"

#include <string>


using std::string;

class Scheduler {
  public:
    Scheduler(const string& owner)
        : mOwner(owner) {}
    ~Scheduler() = default;

    void addEvent(Event* event);
    bool hasEvents(Date date);
    bool performConsistencyCheck(string& message);
    // ...
  private:
    string mOwner;
};

#endif
