# The Case of the Undetectable Side Effect

```Java
// @file AccountDetailFrame.java
public class AccountDetailFrame extends Frame, implements ActionListener, WindowListener
{
    private TextField display = new TextField(10);
    ...
    public AccountDetailFrame(...) { ... }

    public void actionPerformed(ActionEvent event) {
        String source = (String)event.getActionCommand();
        if (source.equals("project activity")) {
            DetailFrame detailDisplay = new DetailFrame();
            detailDisplay.setDescription(
                    getDetailText() + " " + getProjectionText());
            detailDisplay.show();
            String accountDescription = detailDisplay.getAccountSymbol();
            accountDescription += ": ";
            ...
            display.setText(accountDescription);
            ...
        }
    }
    ...
}
// @file AccountDetailFrameTest.java
public void testPerformCommand() {
    AccountDetailFrame frame = new AccountDetailFrame();
    frame.accountSymbol = "SYM";
    frame.performCommand("project activity);
    assertEquals("SYM: basic account", frame.displayText);
}
```