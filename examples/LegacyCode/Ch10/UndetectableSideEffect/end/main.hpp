#pragma once

#include <string>
using String = std::string;

class Frame {
  public:
    virtual ~Frame() = default;
};

class ActionListener {
  public:
    virtual ~ActionListener() = default;
};

class WindowListener {
  public:
    virtual ~WindowListener() = default;
};

class TextField {
    std::string str;

  public:
    TextField(std::string::size_type sz) {
        str.reserve(sz);
    }

    void setText(String text) {}
};

class ActionCommand {
    String name;

  public:
    operator String() {
        return name;
    }
};

class ActionEvent {
  public:
    ActionCommand getActionCommand() {
        return ActionCommand{};
    }
};

class DetailFrame {
  public:
    void setDescription(String description) {}
    void show() {}
    String getAccountSymbol() {
        return "accountSymbol";
    }
};
