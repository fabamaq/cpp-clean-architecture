#include "main.hpp"

#include <catch2/catch.hpp>

class AccountDetailFrame : public Frame, public ActionListener, public WindowListener {
    TextField display = TextField(10);
    DetailFrame detailDisplay; // extracted field #1

  public:
    AccountDetailFrame() = default;
    virtual ~AccountDetailFrame() = default;

    String getDetailText() {
        return "detail";
    }

    String getProjectionText() {
        return "projection";
    }

    void actionPerformed(ActionEvent event) {
        String source = (String)event.getActionCommand();
        performCommand(source);
    }

    void performCommand(String source) {
        if (source == "project activity") {
            // extract method #1
            setDescription(getDetailText() + " " + getProjectionText());
            // ...
            // extract method #2
            String accountDescription = getAccountSymbol();
            accountDescription += ": ";
            // ...
            // extract method #3
            setText(accountDescription);
        }
    }

    // extracted method #1
    virtual void setDescription(String description) {
        // extract field #1
        detailDisplay = DetailFrame();
        detailDisplay.setDescription(description);
        detailDisplay.show();
    }

    // extracted method #2
    virtual String getAccountSymbol() {
        return detailDisplay.getAccountSymbol();
    }

    // extracted method #3
    virtual void setText(String description) {
        display.setText(description);
    }
};

class TestingAccountDetailFrame : public AccountDetailFrame {
  public:
    String displayText = "";
    String accountSymbol = "";

    void setDescription(String description) override {}
    String getAccountSymbol() override {
        return accountSymbol;
    }
    void setText(String text) {
        displayText = text;
    }
};

TEST_CASE("The Case of the Undetectable Side Effect", "[begin]") {
    TestingAccountDetailFrame* frame = new TestingAccountDetailFrame();

    frame->accountSymbol = "SYM";
    frame->performCommand("project activity");

    CHECK(String("SYM: basic account") == frame->displayText);

    delete frame;
}
