#pragma once

#include <string>
using std::string;

#include <vector>

template <typename T = int>
class IList : public std::vector<T> {};

template <typename T>
class ArrayList : public IList<T> {};

struct HttpPostedFile {};

#include <unordered_map>
using HttpFileCollection = std::unordered_map<string, HttpPostedFile>;