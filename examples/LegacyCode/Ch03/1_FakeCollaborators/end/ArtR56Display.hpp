#pragma once

#include <string>

#include "Display.hpp"

class ArtR56Display : public Display {
  public:
	void showLine(std::string line) override;
};
