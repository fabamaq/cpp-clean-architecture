#pragma once

#include <string>

class Display {
  public:
	virtual void showLine(std::string line) = 0;
};
