#include "gtest/gtest.h"

#include "Sale.hpp"

class FakeDisplay : public Display {
  public:
	void showLine(std::string line) override { lastLine = line; }
	std::string getLastLine() const { return lastLine; }

  private:
	std::string lastLine = "";
};

TEST(SaleTestCase, TestDisplayAnItem) {
	FakeDisplay display;
	Sale sale(display);

	sale.scan("1");
	EXPECT_EQ("Milk $3.99$", display.getLastLine());
}
