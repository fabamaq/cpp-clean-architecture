#pragma once

#include <string>

#include "ArtR56Display.hpp"

class Sale {
  public:
	void scan(std::string barcode);

  private:
	ArtR56Display mDisplay;
};
