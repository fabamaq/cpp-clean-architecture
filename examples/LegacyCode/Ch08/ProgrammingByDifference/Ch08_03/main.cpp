/**
 * @brief Programming by Difference
 */
#include "main.hpp"

/**
 * Strip out the 'from' address of a received mail message and return it, so
 * that it can be used as the 'from' address of the message that is forwarded to
 * list recipients
 */
InternetAddress MessageForwarder::getFromAddress(
	Message message) /* throws MessagingException */ {
	std::string fromAddress = getDefaultFrom();
	if (mProperties.getProperty("anonymous") == "true") {
		fromAddress = "anon-membes@" + getDomain();
	} else {
		std::vector<Address> from = message.getFrom();
		if (from.size() > 0) {
			fromAddress = from[0].toString();
		}
	}
	return InternetAddress(fromAddress);
}

void MessageForwarder::forwardMessage(Message message, Session session) {
	MimeMessage forward = MimeMessage(session);
	forward.setFrom(getFromAddress(message));
}

// // class AnonymousMessageForwarder : public MessageForwarder {
// // 	/*
// //   protected:
// // 	InternetAddress getFromAddress(Message message) override {
// // 		std::string anonymousAddress = "anon-" + listAddress;
// // 		return InternetAddress(anonymousAddress);
// // 	}
// // */
// //   private:
// // 	std::string listAddress;
// // };

void testAnonymous() /* throws Exception */ {
	Properties configuration;
	configuration.setProperty("anonymous", "true");
	Message expectedMessage;
	MessageForwarder forwarder(configuration);
	forwarder.forwardMessage(makeFakeMessage());
	assert(("anon-members@" + forwarder.getDomain() ==
			expectedMessage.getFrom()[0].toString()));
}

int main() { testAnonymous(); }
