/**
 * @file main.cpp
 * @Pedro André Oliveira (ei06125@fe.up.pt)
 * @version 0.1
 * @date 2019-05-22
 *
 * @copyright Copyright (c) 2019
 *
 * @brief Test-Driven Development:
 * 1. Write a Failing Test Case
 * 2. Get It to Compile
 * 3. Make It Pass
 * 4. Remove Duplication
 *
 * 1.Write a Failing Test Case
 * 2. Get It to Compile
 * 3. Make It Pass
 * 4. Remove Duplication
 */

#define CATCH_CONFIG_MAIN
#include "../include/catch.hpp"

#include <cassert>

#include <exception>
#include <iostream>
#include <stdexcept>
#include <vector>

using namespace std;

class InvalidBasisException : public std::exception
{
    const char *_what_arg;

public:
    virtual ~InvalidBasisException() {}
    // explicit InvalidBasisException(std::string what_arg) : _what_arg(what_arg.c_str()) {}
    explicit InvalidBasisException(const char *what_arg) : _what_arg(what_arg) {}
    virtual const char *what() const noexcept
    {
        return _what_arg;
    }
};

class InstrumentCalculator
{
    vector<double> elements;

public:
    void addElement(double value)
    {
        elements.push_back(value);
    }

    double firstMomentAbout(double point)
    {
        if (elements.size() == 0)
            throw InvalidBasisException("no elements");

        double numerator = 0.0;
        for (auto element = elements.begin(); element != elements.end(); ++element)
        {
            numerator += *element - point;
        }

        return numerator / elements.size();
    }
};

TEST_CASE("testFirstMoment")
{
    InstrumentCalculator calculator = InstrumentCalculator();
    calculator.addElement(1.0);
    calculator.addElement(2.0);
    assert(calculator.firstMomentAbout(2.0) == -0.5);

    try
    {
        (new InstrumentCalculator())->firstMomentAbout(0.0);
    }
    catch (const InvalidBasisException &e)
    {
        SUCCEED();
    }
    catch (...)
    {
        FAIL();
    }
}
