/**
 * @file main.cpp
 * @Pedro André Oliveira (ei06125@fe.up.pt)
 * @version 0.1
 * @date 2019-05-22
 * 
 * @copyright Copyright (c) 2019
 * 
 * @brief Test-Driven Development:
 * 1. Write a Failing Test Case
 * 2. Get It to Compile
 */

#include <cassert>

class InstrumentCalculator
{
public:
    void addElement(double value)
    {
    }

    double firstMomentAbout(double point)
    {
        return 0.0;
    }
};

void testFirstMoment()
{
    InstrumentCalculator calculator = InstrumentCalculator();
    calculator.addElement(1.0);
    calculator.addElement(2.0);

    assert(calculator.firstMomentAbout(2.0) == -0.5);
}

int main()
{
    testFirstMoment();
    return 0;
}