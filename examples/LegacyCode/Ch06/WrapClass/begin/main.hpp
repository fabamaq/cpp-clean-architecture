#pragma once

#include <ctime>

#include <algorithm>
#include <list>
#include <map>
#include <vector>

class Money {
  double amount;

public:
  void add(double value) { amount += value; }
};

class Timecard {
  double hours;

public:
  double getHours() { return hours; }
};

template <class T> class HashList : std::list<T> {
public:
  bool contains(T value) {
    auto it = std::find(this->begin(), this->end(), value);
    return it != this->end();
  }
};

class PayDispatcher {
public:
  void pay(Employee* employee, tm date, Money amount) {}
};

class Employee {
  std::vector<Timecard> timecards;
  HashList<tm> payPeriod;
  tm date;
  double payRate;

  PayDispatcher payDispatcher;

public:
  void pay();
};