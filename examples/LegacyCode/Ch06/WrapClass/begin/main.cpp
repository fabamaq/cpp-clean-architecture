#include "main.hpp"

void Employee::pay() {
  Money amount = Money();
  // for (Iterator it = timecards.iterator(); it.hasNext();)
  for (auto it = timecards.begin(); it != timecards.end(); ++it) {
    Timecard card = (Timecard)*it;
    if (payPeriod.contains(date)) {
      amount.add(card.getHours() * payRate);
    }
  }
  payDispatcher.pay(this, date, amount);
}
