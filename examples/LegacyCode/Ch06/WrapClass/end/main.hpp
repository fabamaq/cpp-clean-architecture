#pragma once

#include <ctime>

#include <algorithm>
#include <list>
#include <map>
#include <vector>

class Money {
  double amount;

public:
  void add(double value) { amount += value; }
};

class Timecard {
  double hours;

public:
  double getHours() { return hours; }
};

template <class T> class HashList : std::list<T> {
public:
  bool contains(T value) {
    auto it = std::find(this->begin(), this->end(), value);
    return it != this->end();
  }
};

class PayDispatcher {
public:
  void pay(Employee *employee, tm date, Money amount) {}
};

/// @note
/// @brief Employee's interface
class Employee {
public:
  virtual void pay() = 0;
};

/// @brief Abstract Employee
class ConcreteEmployee : public Employee {
  std::vector<Timecard> timecards;
  HashList<tm> payPeriod;
  tm date;
  double payRate;

  PayDispatcher payDispatcher;

public:
  void pay() {
    Money amount = Money();
    // for (Iterator it = timecards.iterator(); it.hasNext();)
    for (auto it = timecards.begin(); it != timecards.end(); ++it) {
      Timecard card = (Timecard)*it;
      if (payPeriod.contains(date)) {
        amount.add(card.getHours() * payRate);
      }
    }
    payDispatcher.pay(this, date, amount);
  }
};
