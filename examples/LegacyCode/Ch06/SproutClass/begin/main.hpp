#pragma once

#include <string>
#include <vector>

struct Result {
  std::string department;
  std::string manager;
  int netProfit;
  int operatingExpense;
};
