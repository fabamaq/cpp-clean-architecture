/**
 * @brief Sprout Class
 *
 * Essentially, two cases lead us to Sprout Class:
 * 1. Your changes lead you toward adding an entirely
 * ... new responsibility to one of your classes.
 * 2. We have a small bit of functionality that
 * ... we could place in an existing class but,
 * ... we can't get that class into a test harness.
 * ----------------------------------------------------------------------------
 *
 * @note Here are the steps for Sprout Class:
 * 1. Identify where you need to make your code change.
 * 2. If the change can be formulated as
 * ... a single sequence of statements in one place in a method:
 * ... 2.1 - think of a good name for a class that could do that work.
 * ... 2.2 - write code that would create an object of that class in that place
 * ... 2.3 - call a method in it that will do the work that you need to do
 * ... 2.4 - then, comment those lines out
 * 3. Determine what local variables you need from the source method,
 * ... and make them arguments to the classes' constructor.
 * 4. Determine whether the sprouted class will need
 * ... to return values to the source method. If so:
 * ... 4.1 - provide a method in the class that will supply those values,
 * ... 4.2 - add a call to the source method to receive those values.
 * 5. Develop the sprout class using test-driven development.
 * ----------------------------------------------------------------------------
 */

#include "main.hpp"

/// ===========================================================================
/// @note We could formulate the change as a little class and use TDD, like:
///
/// class QuarterlyReportTableHeaderProducer
/// {
/// public:
///     string makeHeader();
/// };
///
/// string QuarterlyReportTableHeaderProducer::makeHeader()
/// {
///     return
///     "<tr><td>Department</td><td>Manager</td><td>Profit</td><td>Expenses</td></tr>";
/// }
///
/// ! but we can do better
/// @todo we can create an interface class and have the code inherit from it
///
/// ===========================================================================

/// @brief Interface for the generate function
class HTMLGenerator {
  public:
	virtual ~HTMLGenerator() = 0;
	virtual std::string generate() = 0;
};

/// @brief new class that generates the header
/// @implements HTMLGenerator::generate function
class QuarterlyReportTableHeaderGenerator : public HTMLGenerator {
  public:
	std::string generate() override;
};

std::string QuarterlyReportTableHeaderGenerator::generate() {
	return "<tr><td>Department</td><td>Manager</td><td>Profit</"
		   "td><td>Expenses</"
		   "td></tr>";
}

/**
 * @brief Created using Test-Driven Development
 * @implements HTMLGenerator::generate function
 */
class QuarterlyReportGenerator : public HTMLGenerator {
	int beginDate;
	int endDate;
	struct DB {
		std::vector<Result> queryResults(int b, int e) {
			return std::vector<Result>();
		}
	} database;

  public:
	std::string generate() override;
};

std::string QuarterlyReportGenerator::generate() {
	std::vector<Result> results = database.queryResults(beginDate, endDate);

	std::string pageText;

	pageText += "<html><head><title>"
				"Quarterly Report"
				"</title></head><body><table>";

	QuarterlyReportTableHeaderGenerator headerGenerator;
	pageText += headerGenerator.generate();

	if (results.size() != 0) {
		for (std::vector<Result>::iterator it = results.begin();
			 it != results.end(); ++it) {
			pageText += "<tr>";
			pageText += "<td>" + it->department + "</td>";
			pageText += "<td>" + it->manager + "</td>";
			char buffer[128];
			sprintf(buffer, "<td>$%d</td>", it->netProfit / 100);
			pageText += std::string(buffer);
			sprintf(buffer, "<td>$%d</td>", it->operatingExpense / 100);
			pageText += std::string(buffer);
			pageText += "</tr>";
		}
	} else {
		pageText += "No results for this period";
	}
	pageText += "</table>";
	pageText += "</body>";
	pageText += "</html>";

	return pageText;
}
