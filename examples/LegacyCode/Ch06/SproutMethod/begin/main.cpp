/**
 * @brief Sprout Method
 * - When you need to add a feature to a system and
 * - ... it can be formulated completely as new code,
 * - ... write the code in a new method.
 * - Call it from the places where the new functionality needs to be.
 */

#include "main.hpp"

class TransactionGate {
public:
  /**
   * @todo:
   * ... add code to verify that none of the new entries
   * ... are already in the transactionBundle before
   * ... posting their date and adding them
   */
  void postEntries(list<Entry> entries) {
    list<Entry> entriesToAdd = list<Entry>();
    for (auto iter = entries.begin(); iter != entries.end(); ++iter) {
      auto &entry = *iter;
      /// ! THIS IS BAD (adding code without tests)
      /*
          if (!transactionBundle.getListmanager().hasEntry(entry)) {
              entry.postDate();
              entriesToAdd.push_back(entry);
          }
      */
      entry.postDate();
    }
    gTransactionBundle.getListManager().push_back(entries);
  }
};
