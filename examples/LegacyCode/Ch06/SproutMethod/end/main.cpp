/**
 * @brief Sprout Method
 * - When you need to add a feature to a system and
 * - ... it can be formulated completely as new code,
 * - ... write the code in a new method.
 * - Call it from the places where the new functionality needs to be.
 */

#include "main.hpp"

class TransactionGate {
  public:
	/// @todo test-driven development 'uniqueEntries'
	list<Entry> uniqueEntries(list<Entry> entries) {
		entries.sort();
		entries.unique();
		list<Entry> result = entries;
		return result;
	}

	void postEntries(list<Entry> entries) {
		/// @note verify that new entries are unique before adding
		list<Entry> entriesToAdd = uniqueEntries(entries);
		for (auto iter = entries.begin(); iter != entries.end(); ++iter) {
			iter->postDate();
		}
		gTransactionBundle.getListManager().push_back(entriesToAdd);
	}
};
