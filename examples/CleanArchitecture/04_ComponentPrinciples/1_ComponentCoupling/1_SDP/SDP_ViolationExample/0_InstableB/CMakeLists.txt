# =============================================================================
# Adds target for Instable Class B of the Violation Example

add_executable(SDP_ViolationExample_ICB)

target_sources(
  SDP_ViolationExample_ICB PRIVATE lib/InstableClassB.cpp src/main.cpp
)

target_include_directories(SDP_ViolationExample_ICB PRIVATE ${ROOT_DIR})

target_link_libraries(
  SDP_ViolationExample_ICB PRIVATE SDP_ViolationExample_SCU
)
