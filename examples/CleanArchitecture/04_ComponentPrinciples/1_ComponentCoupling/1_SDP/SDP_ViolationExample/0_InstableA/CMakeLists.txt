# =============================================================================
# Adds target for Instable Class A of the Violation Example

add_executable(SDP_ViolationExample_ICA)

target_sources(
  SDP_ViolationExample_ICA PRIVATE lib/InstableClassA.cpp src/main.cpp
)

target_include_directories(SDP_ViolationExample_ICA PRIVATE ${ROOT_DIR})

target_link_libraries(SDP_ViolationExample_ICA PRIVATE SDP_ViolationExample_SCU)
