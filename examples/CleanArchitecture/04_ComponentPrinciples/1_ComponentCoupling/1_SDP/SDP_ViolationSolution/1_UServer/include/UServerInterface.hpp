#ifndef USERVER_INTERFACE_HPP
#define USERVER_INTERFACE_HPP

class UServerInterface {
  public:
	virtual void foo() const = 0;
	virtual int getValue() const = 0;
};

#endif // USERVER_INTERFACE_HPP
