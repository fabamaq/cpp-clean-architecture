#include <cstdlib>

#include "examples/CleanArchitecture/04_ComponentPrinciples/1_ComponentCoupling/1_SDP/SDP_ViolationSolution/0_InstableA/lib/InstableClassA.hpp"
#include "examples/CleanArchitecture/04_ComponentPrinciples/1_ComponentCoupling/1_SDP/SDP_ViolationSolution/2_Flexible/lib/FlexibleClassC.hpp"

/// ===========================================================================
/// @brief Application entry point
/// ===========================================================================
int main() {
	FlexibleClassC fcc;
	StableClassU scu(fcc);
	InstableClassA ica(scu);
	ica.run();
	return EXIT_SUCCESS;
}
