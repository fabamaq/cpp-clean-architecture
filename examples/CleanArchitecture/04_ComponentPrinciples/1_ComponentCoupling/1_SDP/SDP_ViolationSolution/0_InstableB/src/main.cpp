#include <cstdlib>

#include "examples/CleanArchitecture/04_ComponentPrinciples/1_ComponentCoupling/1_SDP/SDP_ViolationSolution/0_InstableB/lib/InstableClassB.hpp"

#include <iostream>

constexpr int gMagicNumber{7};

class FlexibleClassC : public UServerInterface {
  public:
	void foo() const override {
		std::cout << "0_InstableB::main::FlexibleClassC::foo()" << std::endl;
	}

	int getValue() const override { return mValue; }

  private:
	int mValue{gMagicNumber};
};

/// ===========================================================================
/// @brief Application entry point
/// ===========================================================================
int main() {
	FlexibleClassC fcc;
	StableClassU scu(fcc);
	InstableClassB icb(scu);
	icb.run();
	return EXIT_SUCCESS;
}
