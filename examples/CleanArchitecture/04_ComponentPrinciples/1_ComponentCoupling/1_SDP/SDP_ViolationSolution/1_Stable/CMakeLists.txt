# =============================================================================
# Adds target for Stable Class U of the Violation Solution

add_library(SDP_ViolationSolution_SCU SHARED)

target_sources(SDP_ViolationSolution_SCU PUBLIC lib/StableClassU.cpp)

target_include_directories(SDP_ViolationSolution_SCU PUBLIC ${ROOT_DIR})

target_link_libraries(
  SDP_ViolationSolution_SCU PUBLIC SDP_ViolationSolution_USI
)
