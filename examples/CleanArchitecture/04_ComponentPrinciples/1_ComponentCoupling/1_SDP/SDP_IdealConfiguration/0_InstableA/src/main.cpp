#include <cstdlib>

#include "examples/CleanArchitecture/04_ComponentPrinciples/1_ComponentCoupling/1_SDP/SDP_IdealConfiguration/0_InstableA/lib/InstableClassA.hpp"

/// ===========================================================================
/// @brief Application entry point
/// ===========================================================================
int main() {
	StableClassU scu;
	InstableClassA ica(scu);
	ica.run();
	return EXIT_SUCCESS;
}
