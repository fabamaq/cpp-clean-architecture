#include <cstdlib>

#include "examples/CleanArchitecture/04_ComponentPrinciples/1_ComponentCoupling/1_SDP/SDP_IdealConfiguration/0_InstableB/lib/InstableClassB.hpp"

/// ===========================================================================
/// @brief Application entry point
/// ===========================================================================
int main() {
	StableClassU scu;
	InstableClassB icb(scu);
	icb.run();
	return EXIT_SUCCESS;
}
